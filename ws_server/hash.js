const { scryptSync } = await import("node:crypto")

// Obsidian private key generation. is then used with AES-GCM to create hashed file name.
// TODO: Understand if can override password mechanism so that all that bullshit won't happen.
var hash = scryptSync(Buffer.from("12343215", "utf8"),Buffer.from("123532134", "utf8"), 32, 
    {
    N: 32768,
    maxmem: 67108864,
    p: 1, 
    r: 8
    });

// var newHash = Buffer.from(hash);
var newHash = hash.slice(hash.byteOffset,hash.byteOffset+hash.byteLength);
console.log("Hash:", hash);
console.log("New Hash:", newHash);