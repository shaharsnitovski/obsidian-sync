// TODO: Understand how to retreive IV from hash

import { WebSocketServer } from 'ws';
import {createServer} from 'https';
import { readFileSync } from 'fs';

const { scryptSync, webcrypto } = await import("node:crypto")

const options = {
  key: readFileSync('stunnel.key'),
  cert: readFileSync('stunnel.pem')
};

const https = createServer(options);
https.listen(13337, () => console.log('Https running on port 13337'));

const wss = new WebSocketServer({ server: https, path: '/' });
const _cryptoKey = scryptSync(Buffer.from("12343215", "utf8"),Buffer.from("123532134", "utf8"), 32, 
    {
    N: 32768,
    maxmem: 67108864,
    p: 1, 
    r: 8
    });
const cryptoKey = _cryptoKey.slice(_cryptoKey.byteOffset,_cryptoKey.byteOffset+_cryptoKey.byteLength);
const keyHash = await webcrypto.subtle.digest("SHA-256", cryptoKey);
console.log("Predicting keyHash: ", keyHash);

const encrypt = async (plaintext, rawkey) => {
  const key = await webcrypto.subtle.importKey(
    "raw",
    rawkey,
    "AES-GCM",
    false,
    ['encrypt']
  );
  await webcrypto.subtle.encrypt(
    "AES-GCM",
    key,
    plaintext
  );
}

const decrypt = async(ciphertext, rawkey) => {
  const key = await webcrypto.subtle.importKey(
    "raw",
    rawkey,
    {name: "AES-GCM"},
    false,
    ['decrypt']
  );

  let result = await webcrypto.subtle.decrypt("AES-GCM", key, ciphertext);
  return result;
}

// encrypt(Buffer.from("TEST"), cryptoKey);
// console.log(await decrypt("59b6903726e76ec0d83b937ddd2f3286c23756f7b1a0a7b976a81f241c017d46785affb1d67f6cf1a34fc34f24d504dc94b445bf829de54c4962b56f3dff1bbf213a5de19643e6d05e69fbea7adf6d6c0185f244ccee9470f3c058a5", cryptoKey))
wss.binaryType = 'arraybuffer';
wss.on('connection', function connection(ws) {
  ws.json = (obj) => ws.send(JSON.stringify(obj));
  ws.on('message', function message(data) {
    console.log("Unparsed: ", data);
    var payload = JSON.parse(data);
    console.log("Received: ", payload);
    if (payload.op === "ping") {
        console.log("[+] Sent pong!");
        // ws.json({"res": "ok", "op": "pong"});
        // ws.send("Hello")
    }
    else if (payload.op === "init") {
        console.log("[+] Sent init");
        ws.json({"res": "ok"});
        ws.json({"res": "ok", "op": "ready", "version": 1});
    }
    else if (payload.op === "push") {
        // TODO: implement pushing to file?
        /*
                    Received:  {
            op: 'push',
            path: 'a0464d7961997c343f0281121f3f374c8359e9bedcb45f29fbb4fdc84628c4b08c6e46',
            extension: 'md',
            hash: 'cd372fb85148700fa88095e3d13c83963ab0fe096f6030c734655452c2ae27ee4dd5d69fe97104ad57eb7058c00434c0870ed2917bf3adc6732fe9ede37cd6693730223bf2b99f403f199783243ae1cb6cf75cdab74168e0bd73fc69',
            ctime: 1674161420087,
            mtime: 1674161420087,
            folder: false,
            deleted: false,
            size: 0,
            pieces: 0
            }
        */
       console.log("[+] Push:", payload);
       ws.json({"res": "ok"});
    }
    else if (payload.op === "size") {
       console.log("[+] Size:", payload);
       // ws.json({"res": "ok", "op": "size"});
    }
    else {
        console.log("[+] Payload ", payload.op, " not supported yet");
        ws.json({"res": "ok"});
    }
  });

  // ws.json({"res": "ok"});
});