from django.apps import AppConfig


class SubscriptionApiConfig(AppConfig):
    name = 'subscription_api'
