from django.apps import AppConfig


class VaultApiConfig(AppConfig):
    name = 'vault_api'
