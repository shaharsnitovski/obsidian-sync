from django.shortcuts import render
from django.http import JsonResponse, HttpResponse

# Create your views here.
def list(request):
    return JsonResponse({
        "vaults": [
            {
                "id": 2,
                "name": "vault2",
                "password": "12343215",
                "salt": "123532134",
                "host": "192.168.1.127:13337",
                "keyhash": "12332145"
            }
        ],
        "shared": [],
    })

def access(request):
    return JsonResponse({})